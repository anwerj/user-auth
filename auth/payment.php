<?php
include 'incs/curl.php';
session_start();

if(!isset($_POST["submit"])){
    exit("Access Error!");
}
extract($_POST);



$t_url="https://api.sandbox.paypal.com/v1/oauth2/token";
$t_data="grant_type=client_credentials";
$t_headers=array("Accept:application/json","Accept-Language:en_US");
$t_uspw='AZccrxBulDLvwzk51hR3Y1kjSGu6v_H7mKsBa849OSfpGL7MKllZ2hGJ-R2Y:EEfYuRAcxKroslyFmMPiZqVz63zjXGAimDzgCyaU1i96GiaCVbf5IUGKq8WB';
$t_credentials=  post_request($t_url,$t_data,TRUE,$t_headers,$t_uspw);  // first curl
$t_result=  json_decode($t_credentials);

if(!isset($t_result->access_token)){
    exit("Access_Token Error!");
    
}//  t_error regarding access_token

$access_token=$t_result->access_token;

$p_data='{
  "intent":"sale",
  "redirect_urls":{
    "return_url":"http://jun.com/jun/auth/handle.php",
    "cancel_url":"http://jun.com/jun/auth/handle.php"
  },
  "payer":{
    "payment_method":"paypal"
  },
  "transactions":[
    {
      "amount":{
        "total":"'.$amount.'",
        "currency":"USD"
      },
      "description":"'.$discription.'"
    }
  ]
}';

$p_url="https://api.sandbox.paypal.com/v1/payments/payment";

$p_headers=array("Content-Type:application/json","Authorization:Bearer $access_token");

$response= post_request($p_url,$p_data,true,$p_headers);

$p_result=json_decode($response);

if($p_result->state!="created"){
    exit("Payment Error");
}//  Payment Error 

$links=$p_result->links[1];
$link=$links->href;
$payment_id=$p_result->id;


$_SESSION["payment_id"]=$payment_id;
$_SESSION["access_token"]=$access_token;

//var_dump($p_result);

header("Location: $link&useraction=commit");



?>
