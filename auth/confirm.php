<?php
include "incs/dbconfig.php";
$mydb=new mydb();

if(!isset($_GET['hash'])){
    header("Location: index.php");
}
if(extract($_GET)){
    $sql="select password from user_registration_temp where id=$userid;";
    $pass=$mydb->get_value($sql);
        
    if($hash==sha1($pass)){
        $sql="insert into user_registration select * from user_registration_temp where id=$userid";
        $con=$mydb->insert($sql);
        $msg= "Registered and verified!";
        $sql="select username from user_registration where id=$userid";
        $username=$mydb->get_value($sql);
        $cookies=  setcookie("userId",$username,NULL);
        
        
    }
    else{
       $msg= "Hash not matched";
    }
    
    
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>        
        <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" media="all" >
        <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="js/languages/jquery.validationEngine-en.js"></script>

        <link href="css/validationEngine.jquery.css" type="text/css" rel="stylesheet" >
        <link href="css/validationEngine.jquery.css" type="text/css" rel="stylesheet" >
        <link href="css/base.css" type="text/css" rel="stylesheet" >
        <link href="css/homepage.css" type="text/css" rel="stylesheet" >
        <script>
        $(document).ready(function(){
        $("#register_form").validationEngine();
        });
        </script>


    </head>
    <body>
        <?php
        include "incs/header.php";
        ?>
			        
        <div  class="holder">
            <?=$msg; ?>
                 
        </div>  
        <!--end of holder-->
        <script type="text/javascript">
            $(document).ready(function(){
                reset=function(){
                    window.location="index.php";
                }
                setTimeout("reset()",5000);
                
                
            });
            
        
        </script>
            
       <?php        include 'incs/footer.php';?>            
    </body>
</html>