<?php
include 'incs/dbconfig.php';



?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>        
        <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" media="all" >
        <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="js/languages/jquery.validationEngine-en.js"></script>

        <link href="css/validationEngine.jquery.css" type="text/css" rel="stylesheet" >
        <link href="css/validationEngine.jquery.css" type="text/css" rel="stylesheet" >
        <link href="css/base.css" type="text/css" rel="stylesheet" >
        <link href="css/homepage.css" type="text/css" rel="stylesheet" >
        <script>
        $(document).ready(function(){
        $("#register_form").validationEngine();
        });
        </script>


    </head>
    <body>
        <?php
        include "incs/header.php";
        ?>
			        
        <div  class="holder">
            <div class="left"></div><!-- left -->
            <div class="right">
                <?php if(isset($userid)){ 
                 $sql="select * from user_registration where username='$userid';";
                $mydb=new mydb();
                $re=$mydb->get_row_assoc($sql);
                extract($re);
                    
                ?>
                <table>
                <tr><td>Username</td><td><?=$username;?><td></tr>
                <tr><td>Name</td><td><?=$first_name;?> <?=$last_name;?><td></tr>
                <tr><td>Email</td><td><?=$email;?></td></tr>
                <tr><td>Question</td><td><?=$enquiry;?></td></tr>
                </table>
                <?php } else { ?>
                <form id="register_form" action="register.php" method="POST" class="form-horizontal">
                <input type="hidden" name="validator" value="">
                <table>
                <tr><td>Username</td><td><input type="text" name="username" value="" class="validate[required]"     ><td></tr>
                <tr><td>First Name</td><td><input type="text" name="first_name" value="" class="validate[required]"     ><td></tr>
                <tr><td>Last Name</td><td><input type="text" name="last_name" value=""  class="validate[required]"      ><td></tr>
                <tr><td>Email</td><td><input type="email" name="email" value="" class="validate[required,custom[email]]" ></td></tr>
                <tr><td>Password</td><td><input id="pass1" type="password" name="pass1" class="validate[required]" ></td></tr>
                <tr><td>Confirm Password</td><td><input type="password" name="pass2" class="validate[required,equals[pass1]]"></td></tr>
                <tr><td>Question</td><td><textarea name="enquiry" ></textarea></td></tr>
                <tr><td></td><td><input name="submit" type="submit"></td></tr>      
                
            </form>     
                <?php } ?>
            </div><!-- right -->
            
        </div>  
        <!--end of container-->

            
       <?php        include 'incs/footer.php';?>            
    </body>
</html>
